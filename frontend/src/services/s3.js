import S3 from "react-aws-s3";
import config from "../config/AwsConfig";

const s3 = async (dirname, file, filename) => {
	const S3Client = new S3(config(dirname));
	const data = await S3Client.uploadFile(file, filename);
	return data;
};

export default s3;
