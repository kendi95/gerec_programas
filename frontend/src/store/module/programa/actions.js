export function addProgramaRequest(programa) {
	return {
		type: "@programa/ADD_PROGRAMAS_REQUEST",
		payload: { programa },
	};
}

export function addProgramaSuccess(programa) {
	return {
		type: "@programa/ADD_PROGRAMAS_SUCCESS",
		payload: { programa },
	};
}
