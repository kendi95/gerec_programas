import { all, put, takeLatest } from "redux-saga/effects";
import { addProgramaSuccess } from "./actions";

export function* addProgramasRequest(action) {
	const { programa } = action.payload;
	yield put(addProgramaSuccess(programa));
}

export default all([
	takeLatest("@programa/ADD_PROGRAMAS_REQUEST", addProgramasRequest),
]);
