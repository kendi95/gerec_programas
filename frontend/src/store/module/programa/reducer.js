import produce from "immer";

const INITIAL_STATE = {
	programa: {},
};

export default function programas(state = INITIAL_STATE, actions) {
	const { type, payload } = actions;
	switch (type) {
		case "@programa/ADD_PROGRAMAS_SUCCESS":
			return produce(state, (draft) => {
				draft.programa = payload.programa;
			});

		default:
			return state;
	}
}
