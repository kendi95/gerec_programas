import { all } from "redux-saga/effects";
import garota from "./garota/saga";
import programa from "./programa/saga";

export default function* rootSaga() {
	return yield all([garota, programa]);
}
