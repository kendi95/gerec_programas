export function addGarotaRequest(garota) {
	return {
		type: "@garota/ADD_GAROTAS_REQUEST",
		payload: { garota },
	};
}

export function addGarotaSuccess(garota) {
	return {
		type: "@garota/ADD_GAROTAS_SUCCESS",
		payload: { garota },
	};
}
