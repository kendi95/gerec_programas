import produce from "immer";

const INITIAL_STATE = {
	garota: {},
};

export default function garotas(state = INITIAL_STATE, actions) {
	const { type, payload } = actions;
	switch (type) {
		case "@garota/ADD_GAROTAS_SUCCESS":
			return produce(state, (draft) => {
				draft.garota = payload.garota;
			});

		default:
			return state;
	}
}
