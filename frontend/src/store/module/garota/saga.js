import { all, put, takeLatest } from "redux-saga/effects";
import { addGarotaSuccess } from "./actions";

export function* addGarotasRequest(action) {
	const { garota } = action.payload;
	yield put(addGarotaSuccess(garota));
}

export default all([
	takeLatest("@garota/ADD_GAROTAS_REQUEST", addGarotasRequest),
]);
