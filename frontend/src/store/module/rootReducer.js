import { combineReducers } from "redux";
import garota from "./garota/reducer";
import programa from "./programa/reducer";

export default combineReducers([garota, programa]);
