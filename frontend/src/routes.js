import React from "react";
import { Route, Switch } from "react-router-dom";
import Garota from "./pages/Garota";
import GarotaDetalhes from "./pages/GarotaDetalhes";
import GarotaProgramas from "./pages/GarotaProgramas";
import NewGarota from "./pages/NewGarota";
import Programas from "./pages/Programas";
import Recovery from "./pages/Recovery";
import SignIn from "./pages/SignIn";

export default function Routes() {
	return (
		<Switch>
			<Route path='/' exact component={Garota} />
			<Route path='/garotas/:id' exact component={GarotaDetalhes} />
			<Route path='/garotas/:id/programas' exact component={GarotaProgramas} />

			<Route path='/programas' exact component={Programas} />

			<Route path='/signin' component={SignIn} />

			<Route path='/recovery' component={Recovery} />

			<Route path='/new-garotas' component={NewGarota} />
		</Switch>
	);
}
