import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import api from "../../services/api";
import { addGarotaRequest } from "../../store/module/garota/actions";
import { GarotaList } from "./styles";

export default function Garota() {
	const history = useHistory();
	const dispatch = useDispatch();
	const [garotas, setGarotas] = useState([]);

	async function getGarotas() {
		const res = await api.get("/garotas");
		setGarotas(res.data);
	}

	function showGarotaDetalhes(garota) {
		dispatch(addGarotaRequest(garota));
		history.push(`/garotas/${garota.id}`, garota);
	}

	useEffect(() => {
		getGarotas();
	});

	return (
		<>
			<GarotaList>
				{garotas &&
					garotas.map((garota) => (
						<li key={garota.id}>
							<Link
								to={`/garotas/${garota.id}`}
								onClick={() => showGarotaDetalhes(garota)}
							>
								<img src={garota.avatar_url} alt={garota.cod_nome} />
								<h2>{garota.cod_nome}</h2>
							</Link>
						</li>
					))}
			</GarotaList>
		</>
	);
}
