import styled from "styled-components";

export const GarotaList = styled.ul`
	list-style: none;
	display: grid;
	grid-gap: 20px;
	grid-template-columns: repeat(3, 1fr);
	margin-top: 30px;

	li {
		display: flex;
		flex-direction: column;
		background: #fff;
		margin: 0 auto;
		cursor: pointer;

		a {
			text-decoration: none;
		}

		img {
			border-radius: 8px;
			height: 400px;
			width: 350px;
			align-self: center;
		}

		h2 {
			font-size: 24px;
			color: #333;
			line-height: 50px;
		}
	}
`;
