import React, { useState } from "react";
import api from "../../services/api";
import { Container, Content, ContentInfo, SearchForm } from "./styles";

export default function Programas() {
	const [isSearched, setIsSearched] = useState(true);
	const [cod_hex, setCod_hex] = useState("");
	const [programa, setPrograma] = useState(null);

	async function handleSubmit(e) {
		e.preventDefault();

		try {
			const res = await api.get("/programas", {
				params: {
					cod_hex,
				},
			});
			if (res.status === 200) {
				setPrograma(res.data);
			}
		} catch (err) {
			alert("Código não encontrado.");
		}
	}

	async function confirmPrograma() {
		try {
			const res = await api.patch(
				"/programas",
				{},
				{
					params: {
						cod_hex,
					},
				}
			);
			if (res.status === 204) {
				alert("Cliente confirmado e liberado.");
			}
		} catch (err) {
			alert("Não foi possível confirmá-lo, tente novamente.");
		}
	}

	return (
		<Container>
			<SearchForm onSubmit={handleSubmit}>
				<input
					placeholder='Código do programa'
					value={cod_hex}
					onChange={(e) => setCod_hex(e.target.value)}
				/>
				<button type='submit'>Pesquisar</button>
			</SearchForm>

			{programa && (
				<Content>
					<ContentInfo>
						<strong>Código: </strong> <span>{programa.cod_hex}</span>
					</ContentInfo>

					<ContentInfo>
						<strong>Cliente: </strong> <span>{programa.cliente.nome}</span>
					</ContentInfo>

					<ContentInfo>
						<strong>Acompanhante: </strong>{" "}
						<span>{programa.garota.cod_nome}</span>
					</ContentInfo>

					<ContentInfo>
						<strong>Especialidades: </strong>{" "}
						<span>{programa.especialidades}</span>
					</ContentInfo>

					<ContentInfo>
						<strong>Valor: </strong> <span>R$ {programa.valor_total}</span>
					</ContentInfo>

					<button onClick={confirmPrograma}>Confirmar</button>
				</Content>
			)}
		</Container>
	);
}
