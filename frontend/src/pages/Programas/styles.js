import { darken, lighten } from "polished";
import styled from "styled-components";

export const Container = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	padding: 5% 20% 5%;
`;

export const SearchForm = styled.form`
	width: 100%;
	display: flex;
	flex-direction: row;
	justify-content: space-between;
	align-items: center;

	> input {
		width: 70%;
		height: 40px;
		padding: 2%;
		font-size: 16px;
		border-radius: 8px;
		border: 1px solid #333;

		&::placeholder {
			color: #555;
		}
	}

	> button {
		width: 20%;
		height: 40px;
		border-radius: 8px;
		background: #ff69b4;
		color: #fff;
		font-weight: bold;
		font-size: 16px;
		border: 0;

		&:hover {
			background: ${darken(0.03, "#ff69b4")};
		}
	}
`;

export const Content = styled.div`
	background: ${lighten(0.04, "#eee")};
	border-radius: 8px;
	border-bottom: 2px solid ${darken(0.03, "#eee")};
	border-right: 2px solid ${darken(0.03, "#eee")};
	padding: 4% 4% 4%;
	width: 900px;
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	margin-top: 10%;

	> button {
		margin-top: 5%;
		width: 40%;
		height: 50px;
		border-radius: 8px;
		background: #ff69b4;
		color: #fff;
		font-weight: bold;
		font-size: 16px;
		border: 0;

		&:hover {
			background: ${darken(0.03, "#ff69b4")};
		}
	}
`;

export const ContentInfo = styled.div`
	width: 100%;
	display: flex;
	flex-direction: row;
	justify-content: center;
	align-items: center;
	border-bottom: 1px solid #999;

	> strong {
		font-size: 18px;
		color: #333;
		line-height: 40px;
		width: 100%;
		text-align: end;
		margin-right: 200px;
	}

	> span {
		font-size: 16px;
		width: 100%;
	}
`;
