import { darken } from "polished";
import styled from "styled-components";

export const Container = styled.div`
	width: 100%;
	padding: 10% 10% 0;

	> form {
		display: flex;
		flex-direction: column;
		justify-content: center;
		align-items: center;
		padding: 5% 28% 5%;

		> strong {
			background: #fff;
			width: 100%;
			color: #333;
			font-size: 16px;
			margin-top: 2%;
		}

		> input {
			margin-top: 1%;
			width: 100%;
			border-radius: 8px;
			height: 40px;
			border: 1px solid #777;
			padding: 0 3% 0;
		}

		> button {
			margin-top: 5%;
			width: 50%;
			height: 40px;
			border-radius: 8px;
			border: 0;
			background: #ff69b4;
			color: #fff;
			font-size: 16px;
			font-weight: bold;
			transition: background 0.2s;

			&:hover {
				background: ${darken(0.03, "#ff69b4")};
			}
		}

		> a {
			text-decoration: none;
			color: #999;
			margin-top: 20px;
			transition: color 0.2s;
			font-size: 16px;

			&:hover {
				color: #555;
			}
		}
	}
`;
