import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";
import api from "../../services/api";
import { Container } from "./styles";

export default function SignIn() {
	const history = useHistory();
	const [email, setEmail] = useState("");
	const [senha, setSenha] = useState("");

	async function handleSubmmit(e) {
		e.preventDefault();
		const res = await api.post("/sessions", { email, senha });
		const { token, id } = res.data;
		const user = {
			id,
			auth: `Bearer ${token}`,
		};
		localStorage.setItem("user", JSON.stringify(user));
		history.push("/new-garotas");
	}

	return (
		<Container>
			<form onSubmit={handleSubmmit}>
				<strong>E-mail</strong>
				<input
					type='email'
					value={email}
					onChange={(e) => setEmail(e.target.value)}
				/>
				<strong>Senha</strong>
				<input
					type='password'
					value={senha}
					onChange={(e) => setSenha(e.target.value)}
				/>
				<button type='submit'>Entrar</button>
				<Link to='/recovery'>Esqueci minha senha</Link>
			</form>
		</Container>
	);
}
