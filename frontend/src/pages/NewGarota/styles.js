import { darken } from "polished";
import styled from "styled-components";

export const Container = styled.div`
	width: 100%;
	padding: 5% 10% 0;

	> form {
		display: flex;
		flex-direction: column;
		justify-content: center;
		align-items: center;
		padding: 0 6% 5%;

		> button {
			margin-top: 5%;
			width: 50%;
			height: 40px;
			border-radius: 8px;
			border: 0;
			background: #ff69b4;
			color: #fff;
			font-size: 16px;
			font-weight: bold;
			transition: background 0.2s;

			&:hover {
				background: ${darken(0.03, "#ff69b4")};
			}
		}
	}
`;

export const Horizontal = styled.div`
	width: 100%;
	display: flex;
	flex-direction: row;
	justify-content: center;
	align-items: center;
`;

export const Vertical = styled.div`
	width: 100%;
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;

	> strong {
		background: #fff;
		width: 90%;
		color: #333;
		font-size: 16px;
		margin-top: 2%;
	}

	> input {
		margin-top: 1%;
		width: 90%;
		border-radius: 8px;
		height: 40px;
		border: 1px solid #777;
		padding: 0 3% 0;
	}

	> div {
		justify-content: center;
		align-items: center;
		width: 90%;
		margin-bottom: 1%;
	}
`;
