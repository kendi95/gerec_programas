import MultiSelect from "@khanacademy/react-multi-select";
import crypto from "crypto";
import React, { useEffect, useState } from "react";
import ImageUploader from "react-images-upload";
import api from "../../services/api";
import S3 from "../../services/s3";
import { Container, Horizontal, Vertical } from "./styles";

export default function NewGarota() {
	const [selectEspec, setSelectEspec] = useState([]);
	const [selectPref, setSelectPref] = useState([]);
	const [preferencias, setPreferencias] = useState([]);
	const [especialidades, setEspecialidades] = useState([]);

	const [avatar_url, setAvatar_url] = useState("");
	const [fotos, setFotos] = useState([]);

	const [cod_nome, setCod_nome] = useState("");
	const [idade, setIdade] = useState(0);
	const [email, setEmail] = useState("");
	const [senha, setSenha] = useState("");
	const [altura, setAltura] = useState("");
	const [peso, setPeso] = useState("");
	const [data_nascimento, setData_nascimento] = useState("");
	const [signo, setSigno] = useState("");
	const [valor_hora, setValor_hora] = useState(0);
	const [porcentagem, setPorcentagem] = useState(0.0);

	const user = JSON.parse(localStorage.getItem("user"));

	let optionPref = [];
	let optionEspec = [];

	let picts = [];

	preferencias.map((pref) => {
		optionPref.push({
			value: pref.id,
			label: pref.tipo,
		});
	});

	especialidades.map((espec) => {
		optionEspec.push({
			value: espec.id,
			label: espec.tipo,
		});
	});

	useEffect(() => {
		async function getEspecAndPref() {
			const [pref, espec] = await Promise.all([
				api.get("/preferencias", {
					headers: { Authorization: user.auth },
				}),
				api.get("/especialidades", {
					headers: { Authorization: user.auth },
				}),
			]);
			setPreferencias(pref.data);
			setEspecialidades(espec.data);
		}
		getEspecAndPref();
	}, [user.auth]);

	function setNullFields() {
		setCod_nome("");
		setEmail("");
		setIdade(0);
		setSenha("");
		setAltura(0);
		setPeso(0);
		setData_nascimento("");
		setSigno("");
		setValor_hora("");
		setPorcentagem("");
		setFotos([]);
		setAvatar_url("");
		setSelectEspec([]);
		setSelectPref([]);
	}

	async function handleSubmmit(e) {
		e.preventDefault();

		try {
			const res = await api.post(
				`/users/${user.id}/garotas`,
				{
					cod_nome,
					email,
					senha,
					idade,
					avatar_url,
					altura,
					peso,
					data_nascimento,
					signo,
					valor_hora,
					porcentagem,
					especialidades: selectEspec,
					fotos,
					preferencias: selectPref,
				},
				{
					headers: { Authorization: user.auth },
				}
			);

			setNullFields();

			if (res.status === 201) {
				alert("Cadastro com sucesso.");
			}
		} catch (err) {
			alert("Erro ao cadastrar");
		}
	}

	async function getImage(pictures) {
		const hex = crypto.randomBytes(4).toString("HEX");
		const file = pictures[0];
		const blob = file.slice(0, file.size, "image/png");
		const picture = new File([blob], `${cod_nome}_${hex}`, {
			type: "image/png",
		});

		const res = await S3("avatar", picture, picture.name);
		const { location } = res;
		setAvatar_url(location);
		alert("Avatar enviado.");
	}

	async function getImages(picturess) {
		let pic = null;
		const hex = crypto.randomBytes(4).toString("HEX");
		for (let index = 0; index < picturess.length; index++) {
			const blob = picturess[index].slice(
				0,
				picturess[index].size,
				"image/png"
			);
			pic = new File([blob], `${hex}_${index}`, { type: "image/png" });
			picts.push(pic);
		}

		let pictures = [];
		picts.map(async (picture) => {
			const res = await S3("fotos", picture, picture.name);
			const { location } = res;
			let data = {
				imagem_url: location,
			};
			pictures.push(data);
		});
		setFotos(pictures);
		alert("Fotos enviados.");
	}

	return (
		<Container>
			<form onSubmit={handleSubmmit}>
				<Horizontal>
					<Vertical>
						<strong>Nome: </strong>
						<input
							value={cod_nome}
							onChange={(e) => setCod_nome(e.target.value)}
						/>
					</Vertical>
					<Vertical>
						<strong>Idade:</strong>
						<input
							type='number'
							value={idade}
							onChange={(e) => setIdade(e.target.value)}
						/>
					</Vertical>
				</Horizontal>

				<Horizontal>
					<Vertical>
						<strong>E-mail:</strong>
						<input
							type='email'
							value={email}
							onChange={(e) => setEmail(e.target.value)}
						/>
					</Vertical>
					<Vertical>
						<strong>Senha:</strong>
						<input
							type='password'
							value={senha}
							onChange={(e) => setSenha(e.target.value)}
						/>
					</Vertical>
				</Horizontal>

				<Horizontal>
					<Vertical>
						<strong>Altura:</strong>
						<input
							type='number'
							value={altura}
							onChange={(e) => setAltura(e.target.value)}
						/>
					</Vertical>
					<Vertical>
						<strong>Peso:</strong>
						<input
							type='number'
							value={peso}
							onChange={(e) => setPeso(e.target.value)}
						/>
					</Vertical>
				</Horizontal>

				<Horizontal>
					<Vertical>
						<strong>Data de nascimento:</strong>
						<input
							type='date'
							value={data_nascimento}
							onChange={(e) => setData_nascimento(e.target.value)}
						/>
					</Vertical>
					<Vertical>
						<strong>Signo:</strong>
						<input value={signo} onChange={(e) => setSigno(e.target.value)} />
					</Vertical>
				</Horizontal>

				<Horizontal>
					<Vertical>
						<strong>Valor à hora:</strong>
						<input
							type='number'
							value={valor_hora}
							onChange={(e) => setValor_hora(e.target.value)}
						/>
					</Vertical>
					<Vertical>
						<strong>Porcentagem:</strong>
						<input
							type='number'
							value={porcentagem}
							onChange={(e) => setPorcentagem(e.target.value)}
						/>
					</Vertical>
				</Horizontal>

				<Horizontal>
					<Vertical>
						<strong>Especialidades:</strong>
						<MultiSelect
							options={optionEspec}
							selected={selectEspec}
							onSelectedChanged={(selected) => setSelectEspec(selected)}
						/>
					</Vertical>
					<Vertical>
						<strong>Preferencias:</strong>
						<MultiSelect
							options={optionPref}
							selected={selectPref}
							onSelectedChanged={(selected) => setSelectPref(selected)}
						/>
					</Vertical>
				</Horizontal>

				<Horizontal>
					<Vertical>
						<strong>Avatar:</strong>
						<ImageUploader
							onChange={getImage}
							withPreview={true}
							withIcon={false}
							singleImage={true}
							label=''
							imgExtension={[".jpg", ".png"]}
							buttonText='Escolha uma foto'
						/>
					</Vertical>
					<Vertical>
						<strong>Fotos:</strong>
						<ImageUploader
							onChange={getImages}
							withPreview={true}
							withIcon={false}
							label=''
							imgExtension={[".jpg", ".png"]}
							buttonText='Escolha as fotos'
						/>
					</Vertical>
				</Horizontal>

				<button type='submit'>Cadastrar</button>
			</form>
		</Container>
	);
}
