import { darken } from "polished";
import styled from "styled-components";

export const Container = styled.div`
	width: 100%;
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	padding: 3% 5% 3%;

	a {
		text-decoration: none;
		width: 60%;
		color: #333;
		font-size: 16px;
	}

	> span {
		width: 60%;
		font-size: 16px;
		font-weight: bold;
		color: #333;
		margin-top: 3%;
		margin-bottom: 5px;
	}

	> input {
		width: 60%;
		border: 1px solid #444;
		border-radius: 8px;
		height: 44px;
		padding: 5px;
		font-size: 16px;
		margin-bottom: 8px;
	}

	> div {
		justify-content: center;
		align-items: center;
		width: 60%;
		margin-bottom: 1%;
	}

	> button {
		height: 60px;
		width: 300px;
		background: #ff69b4;
		border-radius: 8px;
		border: 0;
		margin-top: 2%;
		color: #fff;
		font-weight: bold;

		&:hover {
			background: ${darken(0.03, "#ff69b4")};
		}
	}
`;

export const Informacoes = styled.div`
	width: 100%;
	display: flex;
	flex-direction: column;
	margin-top: 10px;

	> strong {
		font-size: 21px;
		color: #222;
		align-self: center;
		line-height: 40px;
		margin: 20px 0 10px;
	}

	> button {
		height: 60px;
		width: 300px;
		background: #ff69b4;
		border-radius: 8px;
		border: 0;
		margin-top: 5%;
		color: #fff;
		font-weight: bold;

		&:hover {
			background: ${darken(0.03, "#ff69b4")};
		}
	}
`;

export const InfoDetalhe = styled.div`
	display: flex;
	flex-direction: row;
	justify-content: center;
	align-items: center;
	width: 100%;
	border-bottom: 1px solid #344;

	> strong {
		font-size: 18px;
		color: #333;
		line-height: 40px;
		width: 100%;
		text-align: end;
		margin-right: 100px;
	}

	> span {
		font-size: 16px;
		width: 100%;
	}
`;
