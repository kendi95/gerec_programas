/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-expressions */
import MultiSelect from "@khanacademy/react-multi-select";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import api from "../../services/api";
import { addProgramaRequest } from "../../store/module/programa/actions";
import { Container, InfoDetalhe, Informacoes } from "./styles";

export default function GarotaPrograma({ match }) {
	const { id } = match.params;
	const { garota } = useSelector((state) => state[0]);
	const { programa } = useSelector((state) => state[1]);

	const [prosseguido, setProsseguido] = useState(false);
	const [select, setSelect] = useState([]);
	const dispatch = useDispatch();

	const [cliente, setCliente] = useState("");
	const [email, setEmail] = useState("");
	const [horas, setHoras] = useState(0);

	let optionEspecs = [];
	let valor = 0;
	let especialidades = [];

	if (garota !== null) {
		garota.especialidades.map((espec) => {
			optionEspecs.push({
				value: espec,
				label: espec,
			});
		});
	}

	// useEffect(() => {
	// 	if (garota !== null) {
	// 		garota.especialidades.map((espec) => {
	// 			optionEspecs.push({
	// 				value: espec,
	// 				label: espec,
	// 			});
	// 		});
	// 	}
	// }, [garota]);

	function total(valor_hora, porcentagem, horas) {
		for (let index = 0; index < especialidades.length; index++) {
			let val_porcentagem = valor_hora * porcentagem;
			let vals = Number(valor_hora) + Number(val_porcentagem);
			let valsMulti = vals * horas;
			valor = valor + valsMulti;
		}
	}

	function handleProsseguir() {
		select.map((sel) => {
			sel.map((value) => {
				especialidades.push(value);
			});
		});
		total(garota.valor_hora, garota.porcentagem, horas);

		const programa = {
			cliente,
			email,
			acompanhante: garota.cod_nome,
			especialidades,
			porcentagem: `${garota.porcentagem * 100}%`,
			valor_hora: garota.valor_hora,
			horas,
			valor_total: valor,
		};
		dispatch(addProgramaRequest(programa));
		setProsseguido(true);
	}

	async function createPrograma() {
		try {
			const res = await api.post(`/garotas/${id}/programas`, {
				nome: programa.cliente,
				email: programa.email,
				especialidades: programa.especialidades,
				valor_total: programa.valor_total,
			});
			if (res.status === 201) {
				alert(`Apresente esse código para a recepção: ${res.data.cod_hex}`);
			}
		} catch (err) {
			alert("Erro ao criar programa");
		}
	}

	return (
		<Container>
			<Link to={`/garotas/${id}`}>Voltar</Link>
			{prosseguido === false ? (
				<>
					<span>Nome do cliente:</span>
					<input value={cliente} onChange={(e) => setCliente(e.target.value)} />
					<span>E-mail:</span>
					<input
						type='email'
						value={email}
						onChange={(e) => setEmail(e.target.value)}
					/>
					<span>Especialidades: </span>
					<MultiSelect
						options={optionEspecs}
						selected={select}
						onSelectedChanged={(selected) => setSelect(selected)}
					/>
					<span>Horas:</span>
					<input
						type='number'
						value={horas}
						onChange={(e) => setHoras(e.target.value)}
					/>
					<button onClick={handleProsseguir}>Prosseguir</button>
				</>
			) : (
				<Informacoes>
					<InfoDetalhe>
						<strong>Cliente: </strong> <span>{programa.cliente}</span>
					</InfoDetalhe>
					<InfoDetalhe>
						<strong>Acompanhante: </strong> <span>{programa.acompanhante}</span>
					</InfoDetalhe>
					<InfoDetalhe>
						<strong>Especialidades: </strong>{" "}
						<span>{programa.especialidades.join(", ")}</span>
					</InfoDetalhe>
					<InfoDetalhe>
						<strong>Porcentagem por especialidades: </strong>{" "}
						<span>{programa.porcentagem}</span>
					</InfoDetalhe>
					<InfoDetalhe>
						<strong>Valor à hora: </strong>{" "}
						<span>R$ {programa.valor_hora}</span>
					</InfoDetalhe>
					<InfoDetalhe>
						<strong>Horas: </strong> <span>{programa.horas}</span>
					</InfoDetalhe>
					<InfoDetalhe>
						<strong>Valor Total: </strong>{" "}
						<span>R$ {programa.valor_total}</span>
					</InfoDetalhe>
					<button onClick={createPrograma}>Confirmar</button>
				</Informacoes>
			)}
		</Container>
	);
}
