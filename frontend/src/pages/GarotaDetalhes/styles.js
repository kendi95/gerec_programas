import { darken } from "polished";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import styled from "styled-components";

export const Container = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	margin: 5% 0 5%;
`;

export const Detalhes = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;

	img {
		border-radius: 8px;
		height: 500px;
		width: 450px;
		align-self: center;
	}

	h1 {
		color: #333;
		line-height: 50px;
		font-size: 32px;
	}
`;

export const Informacoes = styled.div`
	width: 900px;
	display: flex;
	flex-direction: column;
	margin-top: 10px;

	> strong {
		font-size: 21px;
		color: #222;
		align-self: center;
		line-height: 40px;
		margin: 20px 0 10px;
	}
`;

export const InfoDetalhe = styled.div`
	display: flex;
	flex-direction: row;
	justify-content: center;
	align-items: center;
	border-bottom: 1px solid #344;

	> strong {
		font-size: 18px;
		color: #333;
		line-height: 40px;
		width: 100%;
		text-align: end;
		margin-right: 180px;
	}

	> span {
		font-size: 16px;
		width: 100%;
	}
`;

export const ImageCarousel = styled.div`
	width: 50%;
	height: 50%;
`;

export const CarouselImage = styled(Carousel).attrs({
	showArrows: true,
	useKeyboardArrows: true,
	autoPlay: true,
	infiniteLoop: true,
	showStatus: false,
	showIndicators: false,
})`
	align-self: center;
	width: 65%;
	height: 10%;
`;

export const Button = styled.button`
	background: #ff69b4;
	height: 60px;
	width: 300px;
	border-radius: 8px;
	border: 0;
	transition: background 0.2s;

	&:hover {
		background: ${darken(0.03, "#ff69b4")};
	}

	> span {
		font-weight: bold;
		color: #fff;
		font-size: 16px;
	}
`;
