/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Link } from "react-router-dom";
import api from "../../services/api";
import { addGarotaRequest } from "../../store/module/garota/actions";
import {
	Button,
	CarouselImage,
	Container,
	Detalhes,
	InfoDetalhe,
	Informacoes,
} from "./styles";

export default function GarotaDetalhes({ match }) {
	const { id } = match.params;
	const [garota, setGarota] = useState(null);
	const [preferencias, setPreferencias] = useState([]);
	const [especialidades, setEspecialidades] = useState([]);
	const dispatch = useDispatch();

	let prefs = [];
	let especs = [];

	useEffect(() => {
		if (garota === null) {
			async function getDataGarota() {
				const res = await api.get(`/garotas/${id}`);
				setGarota(res.data);
				setPreferencias(res.data.preferencias);
				setEspecialidades(res.data.expecialidades);
			}
			getDataGarota();
		}
	}, [id, garota]);

	useEffect(() => {
		preferencias.map((pref) => {
			prefs.push(pref.tipo);
		});
		especialidades.map((espe) => {
			especs.push(espe.tipo);
		});
	}, [preferencias]);

	return (
		<Container>
			{!garota ? (
				<></>
			) : (
				<>
					<Detalhes>
						<img src={garota.avatar_url} alt='Garota' />
						<h1>{garota.cod_nome}</h1>
					</Detalhes>
					<Informacoes>
						<strong>Informações pessoal </strong>
						<InfoDetalhe>
							<strong>Idade: </strong> <span>{garota.idade}</span>
						</InfoDetalhe>
						<InfoDetalhe>
							<strong>Altura: </strong> <span>{garota.altura}</span>
						</InfoDetalhe>
						<InfoDetalhe>
							<strong>Peso: </strong> <span>{garota.peso}</span>
						</InfoDetalhe>
						<InfoDetalhe>
							<strong>Signo: </strong> <span>{garota.signo}</span>
						</InfoDetalhe>
						<InfoDetalhe>
							<strong>Data de Nascimento: </strong>
							<span>{garota.data_nascimento}</span>
						</InfoDetalhe>
						<InfoDetalhe>
							<strong>Preferências Sexuais: </strong>
							<span>{garota.preferencias.join(", ")}</span>
						</InfoDetalhe>
						<InfoDetalhe>
							<strong>Especialidades: </strong>
							<span>{garota.especialidades.join(", ")}</span>
						</InfoDetalhe>
						<InfoDetalhe>
							<strong>Valor por hora: </strong>{" "}
							<span>R$ {Number(garota.valor_hora)}</span>
						</InfoDetalhe>
						<InfoDetalhe>
							<strong>Porcentagem por especialidades: </strong>{" "}
							<span>{Number(garota.porcentagem * 100)}%</span>
						</InfoDetalhe>

						<strong>Fotos</strong>
						<CarouselImage>
							{garota.fotos.map((foto) => (
								<div key={foto.id}>
									<img src={foto.imagem_url} alt='Garota' />
								</div>
							))}
						</CarouselImage>
					</Informacoes>

					<Link
						to={`/garotas/${id}/programas`}
						onClick={() => dispatch(addGarotaRequest(garota))}
					>
						<Button>
							<span>Programar</span>
						</Button>
					</Link>
				</>
			)}
		</Container>
	);
}
