import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";
import api from "../../services/api";
import { Container } from "./styles";

export default function Recovery() {
	const history = useHistory();
	const [email, setEmail] = useState("");
	const [senha, setSenha] = useState("");
	const [id, setId] = useState(null);

	async function handleSubmit(e) {
		e.preventDefault();
		try {
			if (email) {
				const res = await api.get("/recovery", {
					params: {
						email,
					},
				});
				setId(res.data);
				setEmail("");
			} else if (senha) {
				const res = await api.patch("/recovery", senha, {
					params: {
						id,
					},
				});
				if (res.status === 204) {
					history.push("signin");
				}
			}
		} catch (err) {
			alert("Email não encontrado.");
		}
	}

	return (
		<Container>
			<form onSubmit={handleSubmit}>
				{id === null ? (
					<>
						<strong>E-mail</strong>
						<input
							type='email'
							value={email}
							onChange={(e) => setEmail(e.target.value)}
						/>
					</>
				) : (
					<>
						<strong>Senha</strong>
						<input
							type='password'
							value={senha}
							onChange={(e) => setSenha(e.target.value)}
						/>
					</>
				)}

				<button type='submit'>{id === null ? "Confirmar" : "Salvar"}</button>
				<Link to='/signin'>Fazer login</Link>
			</form>
		</Container>
	);
}
