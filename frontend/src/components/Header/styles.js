import styled from "styled-components";

export const Container = styled.header`
	display: flex;
	flex-direction: row;
	justify-content: space-between;
	align-items: center;
	padding: 15px 20% 15px;
	height: auto;
	background: #eee;

	a {
		display: flex;
		flex-direction: row;
		justify-content: center;
		align-items: center;
		text-decoration: none;

		img {
			width: 50px;
		}

		span {
			font-size: 18px;
			color: #333;
		}
	}
`;
