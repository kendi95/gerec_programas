import React from "react";
import { Link } from "react-router-dom";
import logo from "../../assets/logo.svg";
import { Container } from "./styles";

export default function Header() {
	return (
		<Container>
			<section>
				<Link to='/'>
					<img src={logo} alt='PROGRAMAS' />
					<span>PROGRAMAS</span>
				</Link>
			</section>
		</Container>
	);
}
