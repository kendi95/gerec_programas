import { createBrowserHistory } from "history";
import React from "react";
import { Provider } from "react-redux";
import { Router } from "react-router-dom";
import Header from "./components/Header";
import "./config/ReactotronConfig";
import Route from "./routes";
import store from "./store";
import GlobalStyles from "./styles";

function App() {
	return (
		<Provider store={store}>
			<Router history={createBrowserHistory()}>
				<GlobalStyles />
				<Header />
				<Route />
			</Router>
		</Provider>
	);
}

export default App;
