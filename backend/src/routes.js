import { Router } from 'express';
import Controllers from './app/controllers/Controllers';
import Middlewares from './app/middlewares/Middlewares';

const routes = Router();

//AUTENTICAÇÃO
routes.post(
  '/sessions',
  Middlewares.SessionMiddleware,
  Controllers.SessionController.store
);

//RECOVERY
routes.get('/recovery', Controllers.RecoveryController.show);
routes.patch('/recovery', Controllers.RecoveryController.update);

//USUÁRIO
routes.post(
  '/users',
  Middlewares.UserMiddleware,
  Controllers.UserController.store
);

//GAROTAS
routes.get('/garotas', Controllers.GarotaController.index);
routes.get('/garotas/:id', Controllers.GarotaController.show);

//PROGRAMA
routes.post(
  '/garotas/:id/programas',
  Middlewares.ProgramaMiddleware,
  Controllers.ProgramaController.store
);
routes.get('/programas', Controllers.ProgramaController.show);
routes.patch('/programas', Controllers.ProgramaController.update);

//AUTORIZAÇÃO
routes.use(Middlewares.AuthMiddleware);

//GAROTA
routes.post(
  '/users/:id/garotas',
  Middlewares.GarotaMiddleware,
  Controllers.GarotaController.store
);

//ESPECIALIDADE
routes.post(
  '/especialidades',
  Middlewares.EspecialidadeMiddleware,
  Controllers.EspecialidadeController.store
);
routes.get('/especialidades', Controllers.EspecialidadeController.index);
routes.patch(
  '/especialidades/:id',
  Middlewares.EspecialidadeMiddleware,
  Controllers.EspecialidadeController.update
);
routes.delete(
  '/especialidades/:id',
  Controllers.EspecialidadeController.destroy
);

//PREFERENCIA SEXUAL
routes.post(
  '/preferencias',
  Middlewares.PreferenciaMiddleware,
  Controllers.PreferenciaController.store
);
routes.get('/preferencias', Controllers.PreferenciaController.index);
routes.patch(
  '/preferencias/:id',
  Middlewares.PreferenciaMiddleware,
  Controllers.PreferenciaController.update
);
routes.delete('/preferencias/:id', Controllers.PreferenciaController.destroy);

export default routes;
