import jwt from '../utils/jwt';

export default async (req, res, next) => {
  const { authorization } = req.headers;

  if (!authorization) {
    return res.status(401).json({ error: 'Token requirido.' });
  }

  const [_, token] = authorization.split(' ');

  const decoded = await jwt.decrypt(token);

  if (decoded.name === 'TokenExpiredError' || decoded.name) {
    return res.status(401).json({ error: 'Token expirado.' });
  }

  req.userId = decoded.id;
  next();
};
