import AuthMiddleware from './AuthMiddleware';
import EspecialidadeMiddleware from './EspecialidadeMiddleware';
import GarotaMiddleware from './GarotaMiddleware';
import PreferenciaMiddleware from './PreferenciaMiddleware';
import ProgramaMiddleware from './ProgramaMiddleware';
import SessionMiddleware from './SessionMiddleware';
import UserMiddleware from './UserMiddleware';

export default {
  AuthMiddleware,
  GarotaMiddleware,
  SessionMiddleware,
  UserMiddleware,
  EspecialidadeMiddleware,
  PreferenciaMiddleware,
  ProgramaMiddleware,
};
