import * as Yup from 'yup';

const schema = Yup.object().shape({
  cod_nome: Yup.string().required(),
  email: Yup.string()
    .email()
    .required(),
  senha: Yup.string()
    .min(6)
    .required(),
  idade: Yup.number().required(),
  avatar_url: Yup.string().required(),
  altura: Yup.number().required(),
  peso: Yup.number().required(),
  data_nascimento: Yup.date().required(),
  signo: Yup.string().required(),
  valor_hora: Yup.number().required(),
  porcentagem: Yup.number().required(),
  especialidades: Yup.array().of(Yup.number()),
  fotos: Yup.array().of(
    Yup.object().shape({
      imagem_url: Yup.string().required(),
    })
  ),
  preferencias: Yup.array().of(Yup.number()),
});

export default async (req, res, next) => {
  if (!(await schema.isValid(req.body))) {
    return res.status(400).json({ error: 'Erro de validação.' });
  }
  next();
};
