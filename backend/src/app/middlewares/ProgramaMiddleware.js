import * as Yup from 'yup';

const schema = Yup.object().shape({
  nome: Yup.string().required(),
  email: Yup.string()
    .email()
    .required(),
  especialidades: Yup.string().required(),
  valor_total: Yup.number().required(),
});

export default async (req, res, next) => {
  if (!(await schema.isValid(req.body))) {
    return res.status(400).json({ error: 'Erro de validação.' });
  }
  next();
};
