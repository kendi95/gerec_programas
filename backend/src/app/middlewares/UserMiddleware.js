import * as Yup from 'yup';

const schema = Yup.object().shape({
  nome: Yup.string().required(),
  email: Yup.string()
    .email()
    .required(),
  senha: Yup.string()
    .min(6)
    .required(),
});

export default async (req, res, next) => {
  if (!(await schema.isValid(req.body))) {
    return res.status(400).json({ error: 'Erro de validação.' });
  }
  next();
};
