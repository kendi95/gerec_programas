import Preferencia from '../database/models/Preferencia';

class PreferenciaController {
  async index(req, res) {
    const prefs = await Preferencia.findAll({
      attributes: ['id', 'tipo'],
    });
    return res.status(200).json(prefs);
  }

  async show(req, res) {}

  async store(req, res) {
    const pref = await Preferencia.create(req.body);
    return res.status(201).json(pref);
  }

  async update(req, res) {
    await Preferencia.update(req.body, {
      where: { id: req.params.id },
    });
    return res.status(204).json();
  }

  async destroy(req, res) {
    await Preferencia.destroy({
      where: { id: req.params.id },
    });
    return res.status(204).json();
  }
}

export default new PreferenciaController();
