import Especialidade from '../database/models/Especialidade';
import Foto from '../database/models/Foto';
import Garota from '../database/models/Garota';
import Preferencia from '../database/models/Preferencia';

class GarotaController {
  async index(req, res) {
    const garotas = await Garota.findAll({
      attributes: ['id', 'cod_nome', 'avatar_url'],
    });
    return res.status(200).json(garotas);
  }

  async show(req, res) {
    const id = Number(req.params.id);

    const fotos = await Foto.findAll({
      where: { garota_id: id },
      attributes: ['id', 'imagem_url'],
    });

    const garota = await Garota.findByPk(id, {
      attributes: [
        'cod_nome',
        'email',
        'idade',
        'avatar_url',
        'altura',
        'peso',
        'data_nascimento',
        'signo',
        'valor_hora',
        'porcentagem',
      ],
      include: [
        {
          model: Especialidade,
          as: 'especialidades',
          attributes: ['id', 'tipo'],
        },
        {
          model: Preferencia,
          as: 'preferencias',
          attributes: ['id', 'tipo'],
        },
      ],
    });

    let prefs = [];
    let especs = [];
    garota.preferencias.map((pref) => {
      prefs.push([pref.tipo]);
    });
    garota.especialidades.map((espec) => {
      especs.push([espec.tipo]);
    });

    const newGarota = {
      cod_nome: garota.cod_nome,
      email: garota.email,
      idade: garota.idade,
      avatar_url: garota.avatar_url,
      altura: garota.altura,
      peso: garota.peso,
      data_nascimento: garota.data_nascimento,
      signo: garota.signo,
      valor_hora: garota.valor_hora,
      porcentagem: garota.porcentagem,
      especialidades: prefs,
      preferencias: especs,
      fotos,
    };

    return res.status(200).json(newGarota);
  }

  async store(req, res) {
    const user_id = req.userId;
    const id = Number(req.params.id);
    const {
      cod_nome,
      email,
      senha,
      idade,
      avatar_url,
      altura,
      peso,
      data_nascimento,
      signo,
      porcentagem,
      valor_hora,
    } = req.body;

    const { fotos, especialidades, preferencias } = req.body;

    if (id !== user_id) {
      return res.status(400).json({ error: 'Operação não autorizado.' });
    }

    const garota = await Garota.create({
      cod_nome,
      email,
      senha,
      idade,
      avatar_url,
      altura,
      peso,
      data_nascimento,
      signo,
      porcentagem,
      valor_hora,
      user_id,
    });

    await fotos.map((foto) => {
      Foto.create({
        imagem_url: foto.imagem_url,
        garota_id: garota.id,
      });
    });

    if (especialidades.length > 0) {
      garota.setEspecialidades(especialidades);
    }

    if (preferencias.length > 0) {
      garota.setPreferencias(preferencias);
    }

    return res.status(201).json(garota);
  }

  async update() {}

  async destroy() {}
}

export default new GarotaController();
