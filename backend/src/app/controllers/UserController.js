import User from '../database/models/User';

class UserController {
  async index() {}

  async show() {}

  async store(req, res) {
    const ifExists = User.findOne({
      where: { email: req.body.email },
    });
    if (!ifExists) {
      return res.status(400).json({ error: 'Usuário já existe ' });
    }
    const user = await User.create(req.body);
    return res.status(201).json(user);
  }

  async update() {}

  async destroy() {}
}

export default new UserController();
