import Especialidade from '../database/models/Especialidade';

class EspecialidadeController {
  async index(req, res) {
    const especialidades = await Especialidade.findAll({
      attributes: ['id', 'tipo'],
    });
    return res.status(200).json(especialidades);
  }

  async show(req, res) {}

  async store(req, res) {
    const especidalidade = await Especialidade.create(req.body);
    return res.status(201).json(especidalidade);
  }

  async update(req, res) {
    await Especialidade.update(req.body, {
      where: { id: req.params.id },
    });
    return res.status(204).json();
  }

  async destroy(req, res) {
    await Especialidade.destroy({
      where: { id: req.params.id },
    });
    return res.status(204).json();
  }
}

export default new EspecialidadeController();
