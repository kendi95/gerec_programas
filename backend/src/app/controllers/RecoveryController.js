import User from '../database/models/User';

class RecoveryController {
  async show(req, res) {
    const { email } = req.query;
    const user = await User.findOne({
      where: {
        email,
      },
      attributes: ['id'],
    });
    if (!user) {
      return res.status(400).json({ error: 'Usuário não existe.' });
    }
    return res.status(200).json(user);
  }

  async update(req, res) {
    const { id } = req.query;

    await User.update(req.body, {
      where: {
        id,
      },
    });
    return res.status(204).json();
  }
}

export default new RecoveryController();
