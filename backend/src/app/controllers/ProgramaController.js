import crypto from 'crypto';
import Cliente from '../database/models/Cliente';
import Garota from '../database/models/Garota';
import Programa from '../database/models/Programa';
import Notification from '../database/schemas/Notification';

class ProgramaController {
  async index() {}

  async show(req, res) {
    const cod_hex = String(req.query.cod_hex);
    const programa = await Programa.findOne({
      where: { cod_hex },
      attributes: ['id', 'cod_hex', 'status', 'especialidades', 'valor_total'],
      include: [
        {
          model: Cliente,
          as: 'cliente',
          attributes: ['id', 'nome', 'email'],
        },
        {
          model: Garota,
          as: 'garota',
          attributes: ['id', 'cod_nome', 'email'],
        },
      ],
    });
    if (!programa) {
      return res.status(400).json({ error: 'Código não encontrado.' });
    }
    return res.status(200).json(programa);
  }

  async store(req, res) {
    const garota_id = Number(req.params.id);
    const { nome, email, especialidades, valor_total } = req.body;
    const cod_hex = crypto.randomBytes(4).toString('HEX');
    const especs = especialidades.join(', ');

    const ifExists = await Cliente.findOne({
      where: { email },
    });
    if (!ifExists) {
      const cliente = await Cliente.create({
        nome,
        email,
      });
      const programa = await Programa.create({
        cod_hex,
        especialidades: especs,
        valor_total,
        garota_id,
        cliente_id: cliente.id,
      });
      await Notification.create({
        cod_hex,
        especialidades: especs,
        cliente_nome: cliente.nome,
        valor_total,
      });
      return res.status(201).json(programa);
    }

    const programa = await Programa.create({
      cod_hex,
      especialidades: especs,
      valor_total,
      garota_id,
      cliente_id: ifExists.id,
    });
    await Notification.create({
      cod_hex,
      especialidades: especs,
      cliente_nome: ifExists.nome,
      valor_total,
    });
    return res.status(201).json(programa);
  }

  async update(req, res) {
    const cod_hex = String(req.query.cod_hex);

    const data = {
      status: 'PAGO',
    };

    await Programa.update(data, {
      where: { cod_hex },
    });
    return res.status(204).json();
  }

  async destroy() {}
}

export default new ProgramaController();
