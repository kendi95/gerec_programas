import Garota from '../database/models/Garota';
import User from '../database/models/User';
import jwt from '../utils/jwt';

class SessionController {
  async store(req, res) {
    const { email, senha } = req.body;
    const user = await User.findOne({
      where: { email },
    });
    const garota = await Garota.findOne({
      where: { email },
    });

    if (user) {
      if (!(await user.checkPassword(senha))) {
        return res.status(400).json({ error: 'Senha incorreta.' });
      }
      const token = await jwt.encrypt(user.id);
      const newUser = {
        id: user.id,
        nome: user.nome,
        email: user.email,
        token,
      };
      return res.status(200).json(newUser);
    } else if (garota) {
      if (!(await garota.checkPassword(senha))) {
        return res.status(400).json({ error: 'Senha incorreta.' });
      }
      const token = await jwt.encrypt(garota.id);
      const newUser = {
        id: garota.id,
        nome: garota.cod_nome,
        email: garota.email,
        token,
      };
      return res.status(200).json(newUser);
    } else {
      return res.status(400).json({ error: 'Não existe esse usuário.' });
    }
  }
}

export default new SessionController();
