import EspecialidadeController from './EspecialidadeController';
import GarotaController from './GarotaController';
import PreferenciaController from './PreferenciaController';
import ProgramaController from './ProgramaController';
import RecoveryController from './RecoveryController';
import SessionController from './SessionController';
import UserController from './UserController';

export default {
  GarotaController,
  SessionController,
  UserController,
  EspecialidadeController,
  PreferenciaController,
  ProgramaController,
  RecoveryController,
};
