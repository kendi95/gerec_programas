import { sign, verify } from 'jsonwebtoken';
import { promisify } from 'util';
import auth from '../config/auth';

export default {
  async encrypt(id) {
    return await promisify(sign)({ id }, auth.secret, {
      expiresIn: auth.expiresIn,
    });
  },
  async decrypt(token) {
    return await promisify(verify)(token, auth.secret);
  },
};
