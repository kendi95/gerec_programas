import mongoose from 'mongoose';

const Notification = new mongoose.Schema({
  cod_hex: {
    type: String,
    required: true,
  },
  especialidades: {
    type: String,
    required: true,
  },
  cliente_nome: {
    type: String,
    required: true,
  },
  valor_total: {
    type: Number,
    required: true,
  },
  read: {
    type: Boolean,
    default: false,
    required: true,
  },
});

export default mongoose.model('Notifications', Notification);
