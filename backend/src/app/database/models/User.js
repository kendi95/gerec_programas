import bcrypt from 'bcryptjs';
import Sequelize, { Model } from 'sequelize';

class User extends Model {
  static init(sequelize) {
    super.init(
      {
        nome: Sequelize.STRING,
        email: Sequelize.STRING,
        senha: Sequelize.STRING,
      },
      {
        sequelize,
        tableName: 'users',
      }
    );

    this.addHook('beforeCreate', async user => {
      user.senha = await bcrypt.hash(user.senha, 10);
      return this;
    });

    this.addHook('beforeUpdate', async user => {
      user.senha = await bcrypt.hash(user.senha, 10);
      return this;
    });

    return this;
  }

  static associate(models) {
    this.hasMany(models.Garota);
  }

  checkPassword(senha) {
    return bcrypt.compare(senha, this.senha);
  }
}

export default User;
