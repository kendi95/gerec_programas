import Sequelize, { Model } from 'sequelize';

class Programa extends Model {
  static init(sequelize) {
    super.init(
      {
        cod_hex: Sequelize.STRING,
        especialidades: Sequelize.STRING,
        valor_total: Sequelize.DECIMAL,
        status: Sequelize.STRING,
      },
      {
        sequelize,
        tableName: 'programas',
      }
    );

    return this;
  }

  static associate(models) {
    this.belongsTo(models.Garota, { foreignKey: 'garota_id', as: 'garota' });
    this.belongsTo(models.Cliente, {
      foreignKey: 'cliente_id',
      as: 'cliente',
    });
  }
}

export default Programa;
