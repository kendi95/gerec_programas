import bcrypt from 'bcryptjs';
import Sequelize, { Model } from 'sequelize';

class Garota extends Model {
  static init(sequelize) {
    super.init(
      {
        cod_nome: Sequelize.STRING,
        email: Sequelize.STRING,
        senha: Sequelize.STRING,
        idade: Sequelize.INTEGER,
        avatar_url: Sequelize.STRING,
        altura: Sequelize.DECIMAL,
        peso: Sequelize.INTEGER,
        data_nascimento: Sequelize.DATEONLY,
        signo: Sequelize.STRING,
        valor_hora: Sequelize.DECIMAL,
        porcentagem: Sequelize.DECIMAL,
      },
      {
        sequelize,
        tableName: 'garotas',
      }
    );

    this.addHook('beforeCreate', async user => {
      user.senha = await bcrypt.hash(user.senha, 10);
      return this;
    });

    this.addHook('beforeUpdate', async user => {
      user.senha = await bcrypt.hash(user.senha, 10);
      return this;
    });

    return this;
  }

  static associate(models) {
    this.belongsTo(models.User, { foreignKey: 'user_id', as: 'user' });
    this.hasMany(models.Foto, { as: 'fotos' });
    this.belongsToMany(models.Especialidade, {
      through: 'garotas_has_especialidades',
      foreignKey: 'garota_id',
      as: 'especialidades',
    });
    this.belongsToMany(models.Preferencia, {
      through: 'garotas_has_preferencias',
      foreignKey: 'garota_id',
      as: 'preferencias',
    });
    this.hasMany(models.Programa);
  }

  checkPassword(senha) {
    return bcrypt.compare(senha, this.senha);
  }
}

export default Garota;
