import Cliente from './Cliente';
import Especialidade from './Especialidade';
import Foto from './Foto';
import Garota from './Garota';
import Preferencia from './Preferencia';
import Programa from './Programa';
import User from './User';

export default [
  Cliente,
  Especialidade,
  Foto,
  Garota,
  Preferencia,
  Programa,
  User,
];
