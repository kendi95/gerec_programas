import Sequelize, { Model } from 'sequelize';

class Preferencia extends Model {
  static init(sequelize) {
    super.init(
      {
        tipo: Sequelize.STRING,
      },
      {
        sequelize,
        tableName: 'preferencias',
      }
    );

    return this;
  }

  static associate(models) {
    this.belongsToMany(models.Garota, {
      through: 'garotas_has_preferencias',
      foreignKey: 'preferencia_id',
      as: 'garotas',
    });
  }
}

export default Preferencia;
