import Sequelize, { Model } from 'sequelize';

class Especialidade extends Model {
  static init(sequelize) {
    super.init(
      {
        tipo: Sequelize.STRING,
      },
      {
        sequelize,
        tableName: 'especialidades',
      }
    );

    return this;
  }

  static associate(models) {
    this.belongsToMany(models.Garota, {
      through: 'garotas_has_especialidades',
      foreignKey: 'especialidade_id',
      as: 'garotas',
    });
  }
}

export default Especialidade;
