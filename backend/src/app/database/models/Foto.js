import Sequelize, { Model } from 'sequelize';

class Foto extends Model {
  static init(sequelize) {
    super.init(
      {
        imagem_url: Sequelize.STRING,
      },
      {
        sequelize,
        tableName: 'fotos',
      }
    );

    return this;
  }

  static associate(models) {
    this.belongsTo(models.Garota, { foreignKey: 'garota_id' });
  }
}

export default Foto;
