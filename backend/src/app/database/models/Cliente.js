import Sequelize, { Model } from 'sequelize';

class Cliente extends Model {
  static init(sequelize) {
    super.init(
      {
        nome: Sequelize.STRING,
        email: Sequelize.STRING,
      },
      {
        sequelize,
        tableName: 'clientes',
      }
    );

    return this;
  }

  static associate(models) {
    this.hasMany(models.Programa);
  }
}

export default Cliente;
