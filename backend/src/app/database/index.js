import mongoose from 'mongoose';
import Sequelize from 'sequelize';
import database from '../config/database';
import mongo from '../config/mongoose';
import Models from './models/Models';

class Database {
  constructor() {
    this.init();
    this.mongoose();
  }

  init() {
    this.connection = new Sequelize(database);
    Models.map(model => model.init(this.connection)).map(
      model => model.associate && model.associate(this.connection.models)
    );
  }

  mongoose() {
    this.mongoConnection = mongoose.connect(mongo.url, {
      useNewUrlParser: true,
      useFindAndModify: true,
      useUnifiedTopology: true,
    });
  }
}

export default new Database();
